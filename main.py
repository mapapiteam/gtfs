from argparse import ArgumentParser, Namespace

from gtfs_dw.loader.loader import load
from gtfs_dw.utils.utils import clear_temp
from gtfs_dw.validator.validator import validate
from gtfs_dw.database.database_handler import drop, create
from main_arg_parser import make_argparser
from gtfs_dw.cleaner.cleaner import clean

if __name__ == "__main__":
    parser: ArgumentParser = make_argparser()
    args: Namespace = parser.parse_args()
    try:
        if args.create:
            create()
            exit(1)

        if args.drop:
            drop()
            exit(1)

        if args.reset:
            drop()
            create()
            exit(1)

        if args.url:
            if args.validate and validate(args.url):
                clean(args.url)
                load(args.url)
                clear_temp()
            else:
                load(args.url)

        if args.multi:
            with open(args.multi) as f:
                for line in f:
                    print(f"Loading: {line}")
                    load(line[:-1])
            exit(1)
    except ValueError as err:
        print(err)
