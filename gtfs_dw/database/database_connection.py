import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv(), override=True, verbose=True)

database_connect_string = f"host='{os.getenv('DB_HOST')}' " \
                          f"dbname='{os.getenv('DB_NAME')}' " \
                          f"user='{os.getenv('DB_USER')}' " \
                          f"password='{os.getenv('DB_PASS')}'"