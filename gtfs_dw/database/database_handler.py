import psycopg2
from gtfs_dw.database.database_connection import database_connect_string


sql_file_path = "gtfs_dw/db/"


def drop():
    load_sql_file(sql_file_path + "gtfs_drop_table.sql")


def create():
    load_sql_file(sql_file_path + "gtfs_create_table.sql")


def load_sql_file(sql_path):
    with open(sql_path) as f:
        execute_script(f.read())


def execute_script(sql):
    conn = None
    try:
        conn = psycopg2.connect(database_connect_string)
        cur = conn.cursor()
        cur.execute(sql)
        conn.commit()
        cur.close()
    except psycopg2.DatabaseError as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()