from argparse import ArgumentParser, Namespace
import gtfs_kit as gk


# Prints some information about GTFS data
def print_and_describe(path_or_url):
    feed = get_feed(path_or_url)
    print(gk.describe(feed))
    print(gk.assess_quality(feed))


# Returns true if there are no issues, returns false otherwise and prints the result to a result.txt file.
def validate(path_or_url):
    feed = get_feed(path_or_url)
    result = gk.validate(feed)
    if result.empty:
        return True
    print(result)
    return False


# Returns a feed from either a url or a path
def get_feed(path_or_url):
    feed = None
    try:
        feed = gk.read_feed(path_or_url, dist_units='km')
    except ValueError:
        if args.url:
            print("URL was not found to be a GTFS file")
        else:
            print("Path did not contain GTFS file")
    return feed


if __name__ == "__main__":
    # Parse arguments
    from arg_parser import make_argparser
    parser: ArgumentParser = make_argparser()
    args: Namespace = parser.parse_args()

    if args.validate:
        validate(args.input)

    if args.print:
        print_and_describe(args.input)
