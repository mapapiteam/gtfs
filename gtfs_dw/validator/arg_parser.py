from argparse import ArgumentParser


def make_argparser() -> ArgumentParser:
    """Return argument parser"""
    arg_parser = ArgumentParser(
        description="Validate/Describe GTFS using gtfs-kit.\n"
    )
    arg_parser.add_argument("input", help="input path GTFS data zip file.")
    arg_parser.add_argument("-v", "--validate", help="Validate the file.", action="store_true")
    arg_parser.add_argument("-p", "--print", help="Print and describe the GTFS data", action="store_true")
    arg_parser.add_argument("-u", "--url", help="The input is not a file path but an url", action="store_true")
    return arg_parser
