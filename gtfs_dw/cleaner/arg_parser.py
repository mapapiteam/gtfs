from argparse import ArgumentParser


def make_argparser() -> ArgumentParser:
    """Return argument parser"""
    arg_parser = ArgumentParser(
        description="Repair and clean feeds.\n"
    )
    arg_parser.add_argument("input", help="input path GTFS data zip file.")
    arg_parser.add_argument("-c", "--clean", help="clean the file.", action="store_true")
    return arg_parser
