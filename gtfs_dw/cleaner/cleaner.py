from argparse import ArgumentParser, Namespace

from gtfs_dw.utils.utils import clear_temp, extract_zip_to_temp
from gtfs_dw.validator.validator import get_feed


def clean(path_or_url):
    clear_temp()


def repair(path_or_url):
    clear_temp()
    extract_zip_to_temp(path_or_url)


def clean_agency(path_or_url):
    feed = get_feed(path_or_url)


if __name__ == "__main__":
    from arg_parser import make_argparser

    # Parse arguments
    parser: ArgumentParser = make_argparser()
    args: Namespace = parser.parse_args()
