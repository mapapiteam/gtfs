import csv

from gtfs_dw.database.database_handler import execute_script
from gtfs_dw.loader.loaders.agency import extract_agency_sql_from_line
from gtfs_dw.loader.loaders.attributions import extract_attributions_sql_from_line
from gtfs_dw.loader.loaders.calendar import extract_calendar_sql_from_line
from gtfs_dw.loader.loaders.calendar_dates import extract_calendar_dates_sql_from_line
from gtfs_dw.loader.loaders.fare_attributes import extract_fare_attributes_sql_from_line
from gtfs_dw.loader.loaders.fare_rules import extract_fare_rules_sql_from_line
from gtfs_dw.loader.loaders.feed_info import extract_feed_info_sql_from_line
from gtfs_dw.loader.loaders.frequencies import extract_frequencies_sql_from_line
from gtfs_dw.loader.loaders.levels import extract_levels_sql_from_line
from gtfs_dw.loader.loaders.pathways import extract_pathways_sql_from_line
from gtfs_dw.loader.loaders.routes import extract_routes_sql_from_line
from gtfs_dw.loader.loaders.shapes import extract_shapes_sql_from_line
from gtfs_dw.loader.loaders.stop_times import extract_stop_times_sql_from_line
from gtfs_dw.loader.loaders.stops import extract_stops_sql_from_line
from gtfs_dw.loader.loaders.transfers import extract_transfers_sql_from_line
from gtfs_dw.loader.loaders.translations import extract_translations_sql_from_line
from gtfs_dw.loader.loaders.trips import extract_trips_sql_from_line
from gtfs_dw.utils.utils import extract_zip_to_temp, clear_temp, temp_dir

# Order is important! Example: calendar must come before trips as there are dependencies!
resources = ["agency", "stops", "routes", "calendar", "calendar_dates", "fare_attributes", "fare_rules", "shapes",
             "trips", "stop_times", "frequencies", "transfers", "feed_info", "levels", "translations",
             "feed_info", "attributions", "pathways"]
skip_list = [""]


def extract_sql_from_line(res_name, line):
    if res_name == "agency":
        return extract_agency_sql_from_line(line)
    elif res_name == "stops":
        return extract_stops_sql_from_line(line)
    elif res_name == "routes":
        return extract_routes_sql_from_line(line)
    elif res_name == "trips":
        return extract_trips_sql_from_line(line)
    elif res_name == "stop_times":
        return extract_stop_times_sql_from_line(line)
    elif res_name == "calendar":
        return extract_calendar_sql_from_line(line)
    elif res_name == "calendar_dates":
        return extract_calendar_dates_sql_from_line(line)
    elif res_name == "fare_attributes":
        return extract_fare_attributes_sql_from_line(line)
    elif res_name == "fare_rules":
        return extract_fare_rules_sql_from_line(line)
    elif res_name == "shapes":
        return extract_shapes_sql_from_line(line)
    elif res_name == "frequencies":
        return extract_frequencies_sql_from_line(line)
    elif res_name == "transfers":
        return extract_transfers_sql_from_line(line)
    elif res_name == "pathways":
        return extract_pathways_sql_from_line(line)
    elif res_name == "levels":
        return extract_levels_sql_from_line(line)
    elif res_name == "translations":
        return extract_translations_sql_from_line(line)
    elif res_name == "feed_info":
        return extract_feed_info_sql_from_line(line)
    elif res_name == "attributions":
        return extract_attributions_sql_from_line(line)
    else:
        print(f"No such file loader! for: {res_name} line: {line}")


def load(url_or_path):
    missing_from_spec = 0
    clear_temp()
    extract_zip_to_temp(url_or_path)

    execute_script(f'insert into feed values(\'{url_or_path}\') on conflict do nothing;')
    increment = 100000
    for res_name in resources:
        if res_name in skip_list:
            print("Skipping: " + res_name)
            continue
        print("Loading: " + res_name)
        try:
            with open(temp_dir + "/" + res_name + ".txt", newline='\n') as file:
                count = 0
                sql_script = ""
                reader = csv.DictReader(file, delimiter=',', quotechar='"')
                for line in reader:
                    count = count + 1
                    sql_script += extract_sql_from_line(res_name, line) + f",'{url_or_path}') on conflict do nothing;\n"
                    if count % increment == 0:
                        print(f"Loading: {count} {res_name}")
                        try:
                            execute_script(sql_script)
                            sql_script = ""
                        except Exception as e:
                            print(e)
                if sql_script != "":
                    execute_script(sql_script)
                print(f"Loaded total: {count} {res_name}")
        except FileNotFoundError as e:
            print(f"Couldn't find {e.filename}")
            missing_from_spec = 1 + missing_from_spec
    print(f"Total missing from spec. {missing_from_spec}")
