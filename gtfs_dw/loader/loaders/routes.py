from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_routes_sql_from_line(line):
    route_id = standard_text_attribute_extract(line, 'route_id')
    agency_id = standard_text_attribute_extract(line, 'agency_id')
    route_short_name = standard_text_attribute_extract(line, 'route_short_name')
    route_long_name = standard_text_attribute_extract(line, 'route_long_name')
    route_desc = standard_text_attribute_extract(line, 'route_desc')
    route_type = standard_number_attribute_extract(line, 'route_type')
    route_url = standard_text_attribute_extract(line, 'route_url')
    route_color = standard_text_attribute_extract(line, 'route_color')
    route_text_color = standard_text_attribute_extract(line, 'route_text_color')
    route_sort_order = standard_text_attribute_extract(line, 'route_sort_order')
    continuous_pickup = standard_text_attribute_extract(line, 'continuous_pickup')
    continuous_drop_off = standard_text_attribute_extract(line, 'continuous_drop_off')

    query = f"insert into routes values ({route_id},{agency_id},{route_short_name},{route_long_name}," \
            f"{route_desc},{route_type},{route_url},{route_color},{route_text_color},{route_sort_order}," \
            f"{continuous_pickup},{continuous_drop_off}"
    return query