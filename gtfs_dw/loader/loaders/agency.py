from gtfs_dw.utils.utils import standard_text_attribute_extract


def extract_agency_sql_from_line(line):
    agency_id = standard_text_attribute_extract(line, 'agency_id')
    agency_name = standard_text_attribute_extract(line, 'agency_name')
    agency_url = standard_text_attribute_extract(line, 'agency_url')
    agency_timezone = standard_text_attribute_extract(line, 'agency_timezone')
    agency_lang = standard_text_attribute_extract(line, 'agency_lang')
    agency_phone = standard_text_attribute_extract(line, 'agency_phone')
    agency_fare_url = standard_text_attribute_extract(line, 'agency_fare_url')
    agency_email = standard_text_attribute_extract(line, 'agency_email')

    query = f"insert into agency values ({agency_id},{agency_name},{agency_url},{agency_timezone}," \
            f"{agency_lang},{agency_phone},{agency_fare_url},{agency_email}"
    return query
