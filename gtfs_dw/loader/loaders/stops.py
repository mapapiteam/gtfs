from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_stops_sql_from_line(line):
    stop_id = standard_text_attribute_extract(line, 'stop_id')
    stop_code = standard_text_attribute_extract(line, 'stop_code')
    stop_name = standard_text_attribute_extract(line, 'stop_name')
    tts_stop_name = standard_text_attribute_extract(line, 'tts_stop_name')
    stop_desc = standard_text_attribute_extract(line, 'stop_desc')
    stop_lat = standard_number_attribute_extract(line, 'stop_lat')
    stop_lon = standard_number_attribute_extract(line, 'stop_lon')
    zone_id = standard_text_attribute_extract(line, 'zone_id')
    stop_url = standard_text_attribute_extract(line, 'stop_url')
    location_type = standard_number_attribute_extract(line, 'location_type')
    parent_station = standard_text_attribute_extract(line, 'parent_station')
    stop_timezone = standard_text_attribute_extract(line, 'stop_timezone')
    wheelchair_boarding = standard_number_attribute_extract(line, 'wheelchair_boarding')
    level_id = standard_text_attribute_extract(line, 'level_id')
    platform_code = standard_text_attribute_extract(line, 'platform_code')

    query = f"insert into stops values ({stop_id},{stop_code},{stop_name},{tts_stop_name}," \
            f"{stop_desc},{stop_lat},{stop_lon},{zone_id},{stop_url},{location_type},{parent_station}," \
            f"{stop_timezone},{wheelchair_boarding},{level_id},{platform_code}"
    return query
