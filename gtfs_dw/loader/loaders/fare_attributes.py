from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_fare_attributes_sql_from_line(line):
    fare_id = standard_text_attribute_extract(line, 'fare_id')
    price = standard_number_attribute_extract(line, 'price')
    currency_type = standard_text_attribute_extract(line, 'currency_type')
    payment_method = standard_number_attribute_extract(line, 'payment_method')
    transfers = standard_number_attribute_extract(line, 'transfers')
    agency_id = standard_text_attribute_extract(line, 'agency_id')
    transfer_duration = standard_number_attribute_extract(line, 'transfer_duration')

    query = f"insert into fare_attributes values ({fare_id},{price},{currency_type},{payment_method}," \
            f"{transfers},{agency_id},{transfer_duration}"
    return query
