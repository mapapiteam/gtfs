from gtfs_dw.utils.utils import standard_date_attribute_extract, standard_number_attribute_extract, \
    standard_text_attribute_extract


def extract_calendar_dates_sql_from_line(line):
    service_id = standard_text_attribute_extract(line, 'service_id')
    date = standard_date_attribute_extract(line, 'date')
    exception_type = standard_number_attribute_extract(line, 'exception_type')

    query = f"insert into calendar_dates values ({service_id},{date},{exception_type}"
    return query
