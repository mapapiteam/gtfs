from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_stop_times_sql_from_line(line):
    trip_id = standard_text_attribute_extract(line, 'trip_id')
    arrival_time = standard_text_attribute_extract(line, 'arrival_time')
    departure_time = standard_text_attribute_extract(line, 'departure_time')
    stop_id = standard_text_attribute_extract(line, 'stop_id')
    stop_headsign = standard_text_attribute_extract(line, 'stop_headsign')
    stop_sequence = standard_number_attribute_extract(line, 'stop_sequence')
    pickup_type = standard_number_attribute_extract(line, 'pickup_type')
    drop_off_type = standard_number_attribute_extract(line, 'drop_off_type')
    continuous_pickup = standard_text_attribute_extract(line, 'continuous_pickup')
    continuous_drop_off = standard_text_attribute_extract(line, 'continuous_drop_off')
    shape_dist_traveled = standard_text_attribute_extract(line, 'shape_dist_traveled')
    timepoint = standard_text_attribute_extract(line, 'timepoint')

    query = f"insert into stop_times values ({trip_id},{arrival_time},{departure_time},{stop_id}," \
            f"{stop_headsign},{stop_sequence},{pickup_type},{drop_off_type},{continuous_pickup}," \
            f"{continuous_drop_off},{shape_dist_traveled},{timepoint}"
    return query
