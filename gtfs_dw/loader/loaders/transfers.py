from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_transfers_sql_from_line(line):
    from_stop_id = standard_text_attribute_extract(line, 'from_stop_id')
    to_stop_id = standard_text_attribute_extract(line, 'to_stop_id')
    transfer_type = standard_number_attribute_extract(line, 'transfer_type')
    min_transfer_time = standard_number_attribute_extract(line, 'min_transfer_time')

    query = f"insert into transfers values ({from_stop_id},{to_stop_id},{transfer_type}" \
            f",{min_transfer_time}"
    return query
