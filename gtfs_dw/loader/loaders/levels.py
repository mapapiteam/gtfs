from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_levels_sql_from_line(line):
    level_id = standard_text_attribute_extract(line, 'level_id')
    level_index = standard_number_attribute_extract(line, 'level_index')
    level_name = standard_text_attribute_extract(line, 'level_name')

    query = f"insert into levels values ({level_id},{level_index},{level_name}"
    return query
