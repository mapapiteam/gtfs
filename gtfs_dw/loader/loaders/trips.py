from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_trips_sql_from_line(line):
    route_id = standard_text_attribute_extract(line, 'route_id')
    service_id = standard_text_attribute_extract(line, 'service_id')
    trip_id = standard_text_attribute_extract(line, 'trip_id')
    trip_headsign = standard_text_attribute_extract(line, 'trip_headsign')
    trip_short_name = standard_text_attribute_extract(line, 'trip_short_name')
    direction_id = standard_number_attribute_extract(line, 'direction_id')
    block_id = standard_text_attribute_extract(line, 'block_id')
    shape_id = standard_text_attribute_extract(line, 'shape_id')
    wheelchair_accessible = standard_text_attribute_extract(line, 'wheelchair_accessible')
    bikes_allowed = standard_text_attribute_extract(line, 'bikes_allowed')

    query = f"insert into trips values ({route_id},{service_id},{trip_id},{trip_headsign}," \
            f"{trip_short_name},{direction_id},{block_id},{shape_id},{wheelchair_accessible},{bikes_allowed}"
    return query
