from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_translations_sql_from_line(line):
    table_name = standard_text_attribute_extract(line, 'table_name')
    field_name = standard_text_attribute_extract(line, 'field_name')
    language = standard_text_attribute_extract(line, 'language')
    translation = standard_text_attribute_extract(line, 'translation')
    record_id = standard_text_attribute_extract(line, 'record_id')
    record_sub_id = standard_text_attribute_extract(line, 'record_sub_id')
    field_value = standard_text_attribute_extract(line, 'field_value')

    query = f"insert into levels values ({table_name},{field_name},{language},{translation},{record_id}," \
            f"{record_sub_id},{field_value}"
    return query
