from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_date_attribute_extract


def extract_feed_info_sql_from_line(line):
    feed_publisher_name = standard_text_attribute_extract(line, 'feed_publisher_name')
    feed_publisher_url = standard_text_attribute_extract(line, 'feed_publisher_url')
    feed_lang = standard_text_attribute_extract(line, 'feed_lang')
    default_lang = standard_text_attribute_extract(line, 'default_lang')
    feed_start_date = standard_date_attribute_extract(line, 'feed_start_date')
    feed_end_date = standard_date_attribute_extract(line, 'feed_end_date')
    feed_version = standard_text_attribute_extract(line, 'feed_version')
    feed_contact_email = standard_text_attribute_extract(line, 'feed_contact_email')
    feed_contact_url = standard_text_attribute_extract(line, 'feed_contact_url')

    query = f"insert into feed_info values ({feed_publisher_name},{feed_publisher_url},{feed_lang}," \
            f"{default_lang},{feed_start_date},{feed_end_date},{feed_version},{feed_contact_email}," \
            f"{feed_contact_url}"
    return query
