from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract, \
    standard_date_attribute_extract


def extract_calendar_sql_from_line(line):
    service_id = standard_text_attribute_extract(line, 'service_id')
    monday = standard_number_attribute_extract(line, 'monday')
    tuesday = standard_number_attribute_extract(line, 'tuesday')
    wednesday = standard_number_attribute_extract(line, 'wednesday')
    thursday = standard_number_attribute_extract(line, 'thursday')
    friday = standard_number_attribute_extract(line, 'friday')
    saturday = standard_number_attribute_extract(line, 'saturday')
    sunday = standard_number_attribute_extract(line, 'sunday')
    start_date = standard_date_attribute_extract(line, 'start_date')
    end_date = standard_date_attribute_extract(line, 'end_date')

    query = f"insert into calendar values ({service_id},{monday},{tuesday},{wednesday}," \
            f"{thursday},{friday},{saturday},{sunday},{start_date},{end_date}"
    return query
