from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_frequencies_sql_from_line(line):
    trip_id = standard_text_attribute_extract(line, 'trip_id')
    start_time = standard_text_attribute_extract(line, 'start_time')
    end_time = standard_text_attribute_extract(line, 'end_time')
    headway_secs = standard_number_attribute_extract(line, 'headway_secs')
    exact_times = standard_number_attribute_extract(line, 'exact_times')

    query = f"insert into frequencies values ({trip_id},{start_time},{end_time},{headway_secs},{exact_times}"
    return query
