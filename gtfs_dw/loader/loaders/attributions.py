from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_attributions_sql_from_line(line):
    attribution_id = standard_text_attribute_extract(line, 'attribution_id')
    agency_id = standard_text_attribute_extract(line, 'agency_id')
    route_id = standard_text_attribute_extract(line, 'route_id')
    trip_id = standard_text_attribute_extract(line, 'trip_id')
    organization_name = standard_text_attribute_extract(line, 'organization_name')
    is_producer = standard_number_attribute_extract(line, 'is_producer')
    is_operator = standard_number_attribute_extract(line, 'is_operator')
    is_authority = standard_number_attribute_extract(line, 'is_authority')
    attribution_url = standard_text_attribute_extract(line, 'attribution_url')
    attribution_email = standard_text_attribute_extract(line, 'attribution_email')
    attribution_phone = standard_text_attribute_extract(line, 'attribution_phone')
    feed_id = standard_text_attribute_extract(line, 'feed_id')

    query = f"insert into attributions values ({attribution_id},{agency_id},{route_id},{trip_id},{organization_name}," \
            f"{is_producer},{is_operator},{is_authority},{attribution_url},{attribution_email},{attribution_phone}"
    return query
