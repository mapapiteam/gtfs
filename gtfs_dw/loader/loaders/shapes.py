from gtfs_dw.utils.utils import standard_number_attribute_extract, standard_text_attribute_extract


def extract_shapes_sql_from_line(line):
    shape_id = standard_text_attribute_extract(line, 'shape_id')
    shape_pt_lat = standard_number_attribute_extract(line, 'shape_pt_lat')
    shape_pt_lon = standard_number_attribute_extract(line, 'shape_pt_lon')
    shape_pt_sequence = standard_number_attribute_extract(line, 'shape_pt_sequence')
    shape_dist_traveled = standard_number_attribute_extract(line, 'shape_dist_traveled')

    query = f"insert into shapes values ({shape_id},{shape_pt_lat},{shape_pt_lon},{shape_pt_sequence}," \
            f"{shape_dist_traveled}"
    return query
