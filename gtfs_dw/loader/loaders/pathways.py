from gtfs_dw.utils.utils import standard_text_attribute_extract, standard_number_attribute_extract


def extract_pathways_sql_from_line(line):
    pathway_id = standard_text_attribute_extract(line, 'pathway_id')
    from_stop_id = standard_text_attribute_extract(line, 'from_stop_id')
    to_stop_id = standard_text_attribute_extract(line, 'to_stop_id')
    pathway_mode = standard_number_attribute_extract(line, 'pathway_mode')
    is_bidirectional = standard_number_attribute_extract(line, 'is_bidirectional')
    length = standard_number_attribute_extract(line, 'length')
    traversal_time = standard_number_attribute_extract(line, 'traversal_time')
    stair_count = standard_number_attribute_extract(line, 'stair_count')
    max_slope = standard_number_attribute_extract(line, 'max_slope')
    min_width = standard_text_attribute_extract(line, 'min_width')
    signposted_as = standard_text_attribute_extract(line, 'signposted_as')
    reversed_signposted_as = standard_text_attribute_extract(line, 'reversed_signposted_as')

    query = f"insert into levels values ({pathway_id},{from_stop_id},{to_stop_id},{pathway_mode},{is_bidirectional}," \
            f"{length},{traversal_time},{stair_count},{max_slope},{min_width},{signposted_as},{reversed_signposted_as}"
    return query
