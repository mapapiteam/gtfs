from gtfs_dw.utils.utils import standard_text_attribute_extract


def extract_fare_rules_sql_from_line(line):
    fare_id = standard_text_attribute_extract(line, 'fare_id')
    route_id = standard_text_attribute_extract(line, 'route_id')
    origin_id = standard_text_attribute_extract(line, 'origin_id')
    destination_id = standard_text_attribute_extract(line, 'destination_id')
    contains_id = standard_text_attribute_extract(line, 'contains_id')

    query = f"insert into fare_rules values ({fare_id},{route_id},{origin_id},{destination_id},{contains_id}"
    return query
