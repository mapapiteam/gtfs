--------------------------------------------------------------------------------
-- drops all tables related to GTFS
--------------------------------------------------------------------------------
drop table if exists agency cascade;
drop table if exists stops cascade;
drop table if exists routes cascade;
drop table if exists trips cascade;
drop table if exists stop_times cascade;
drop table if exists calendar cascade;
drop table if exists calendar_dates cascade;
drop table if exists fare_attributes cascade;
drop table if exists fare_rules cascade;
drop table if exists shapes cascade;
drop table if exists frequencies cascade;
drop table if exists transfers cascade;
drop table if exists feed_info cascade;
drop table if exists attributions cascade;
drop table if exists levels cascade;
drop table if exists pathways cascade;
drop table if exists translations cascade;


--------------------------------------------------------------------------------
-- Static tables, not directly in the standard by specified as enums
--------------------------------------------------------------------------------
drop table if exists route_types cascade;
drop table if exists directions cascade;
drop table if exists pickup_dropoff_types cascade;
drop table if exists payment_methods cascade;
drop table if exists location_types cascade;
drop table if exists wheelchair_boardings cascade;
drop table if exists transfer_types cascade;

--------------------------------------------------------------------------------
-- drops meta tables
--------------------------------------------------------------------------------
drop table if exists feed cascade;
