--------------------------------------------------------------------------------
-- create tables
-- Table names from
-- Column names and ordering taken from http://gtfs.org/reference/static
-- TODO add courrency codes https://datahub.io/JohnSnowLabs/iso-4217-currency-codes
-- TODO add Language Code as table see https://datahub.io/core/language-codes#resource-language-codes


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Meta table for loading multiple feeds
--------------------------------------------------------------------------------
create table feed (
     feed_id  text primary key
);

--------------------------------------------------------------------------------
-- Static tables, not directly in the standard by specified as enums
--------------------------------------------------------------------------------
create table location_types (
    location_type int  primary key
    ,description  text not null
);

insert into location_types values (0,'Stop (or Platform)');
insert into location_types values (1,'Station');
insert into location_types values (2,'Entrance/Exit');
insert into location_types values (3,'Generic Node');
insert into location_types values (4,'Boarding Area');

create table wheelchair_boardings (
    wheelchair_boarding int primary key
    ,description        text not null
);
insert into wheelchair_boardings values (0, 'No accessibility information available for the stop');
insert into wheelchair_boardings values (1, 'At least some vehicles at this stop can be boarded by a rider in a wheelchair');
insert into wheelchair_boardings values (2, 'Wheelchair boarding is not possible at this stop');

create table directions (
    direction_id int  primary key
    ,description text not null
);

insert into directions values (0,'This way');
insert into directions values (1,'That way');

create table pickup_dropoff_types (
    type_id      int primary key
    ,description text
);

-- TODO check values with http://gtfs.org/reference/static#agencytxt
insert into pickup_dropoff_types values (0,'Regularly Scheduled');
insert into pickup_dropoff_types values (1,'Not available');
insert into pickup_dropoff_types values (2,'Phone arrangement only');
insert into pickup_dropoff_types values (3,'Driver arrangement only');

create table route_types (
    route_type   int  primary key
    ,description text not null
);
insert into route_types values (0, 'Tram, Streetcar, Light rail. Any light rail or street level system within a metropolitan area.');
insert into route_types values (1, 'Subway, Metro. Any underground rail system within a metropolitan area.');
insert into route_types values (2, 'Rail. Used for intercity or long-distance travel.');
insert into route_types values (3, 'Bus. Used for short- and long-distance bus routes.');
insert into route_types values (4, 'Ferry. Used for short- and long-distance boat service.');
insert into route_types values (5, 'Cable tram. Used for street-level rail cars where the cable runs beneath the vehicle, e.g., cable car in San Francisco.');
insert into route_types values (6, 'Aerial lift, suspended cable car (e.g., gondola lift, aerial tramway). Cable transport where cabins, cars, gondolas or open chairs are suspended by means of one or more cables.');
insert into route_types values (7, 'Funicular. Any rail system designed for steep inclines.');
insert into route_types values (11, 'Trolleybus. Electric buses that draw power from overhead wires using poles.');
insert into route_types values (12, 'Monorail. Railway in which the track consists of a single rail or a beam.');
insert into route_types values (100, 'Railway Service');
insert into route_types values (101, 'High Speed Rail Service');
insert into route_types values (102, 'Long Distance Trains');
insert into route_types values (103, 'Inter Regional Rail Service');
insert into route_types values (104, 'Car Transport Rail Service');
insert into route_types values (105, 'Sleeper Rail Service');
insert into route_types values (106, 'Regional Rail Service');
insert into route_types values (107, 'Tourist Railway Service');
insert into route_types values (108, 'Rail Shuttle (Within Complex)');
insert into route_types values (109, 'Suburban Railway');
insert into route_types values (110, 'Replacement Rail Service');
insert into route_types values (111, 'Special Rail Service');
insert into route_types values (112, 'Lorry Transport Rail Service');
insert into route_types values (113, 'All Rail Services');
insert into route_types values (114, 'Cross-Country Rail Service');
insert into route_types values (115, 'Vehicle Transport Rail Service');
insert into route_types values (116, 'Rack and Pinion Railway');
insert into route_types values (117, 'Additional Rail Service');
insert into route_types values (200, 'Coach Service');
insert into route_types values (201, 'International Coach Service');
insert into route_types values (202, 'National Coach Service');
insert into route_types values (203, 'Shuttle Coach Service');
insert into route_types values (204, 'Regional Coach Service');
insert into route_types values (205, 'Special Coach Service');
insert into route_types values (206, 'Sightseeing Coach Service');
insert into route_types values (207, 'Tourist Coach Service');
insert into route_types values (208, 'Commuter Coach Service');
insert into route_types values (209, 'All Coach Services');
insert into route_types values (400, 'Urban Railway Service');
insert into route_types values (401, 'Metro Service');
insert into route_types values (402, 'Underground Service');
insert into route_types values (403, 'Urban Railway Service');
insert into route_types values (404, 'All Urban Railway Services');
insert into route_types values (405, 'Monorail');
insert into route_types values (700, 'Bus Service');
insert into route_types values (701, 'Regional Bus Service');
insert into route_types values (702, 'Express Bus Service');
insert into route_types values (703, 'Stopping Bus Service');
insert into route_types values (704, 'Local Bus Service');
insert into route_types values (705, 'Night Bus Service');
insert into route_types values (707, 'Special Needs Bus');
insert into route_types values (708, 'Mobility Bus Service');
insert into route_types values (709, 'Mobility Bus for Registered Disabled');
insert into route_types values (710, 'Sightseeing Bus');
insert into route_types values (711, 'Shuttle Bus');
insert into route_types values (712, 'School Bus');
insert into route_types values (713, 'School and Public Service Bus');
insert into route_types values (714, 'Rail Replacement Bus Service');
insert into route_types values (715, 'Demand and Response Bus Service');
insert into route_types values (716, 'All Bus Services');
insert into route_types values (800, 'Trolleybus Service');
insert into route_types values (900, 'Tram Service');
insert into route_types values (901, 'City Tram Service');
insert into route_types values (902, 'Local Tram Service');
insert into route_types values (903, 'Regional Tram Service');
insert into route_types values (904, 'Sightseeing Tram Service');
insert into route_types values (905, 'Shuttle Tram Service');
insert into route_types values (1000, 'Water Transport Service');
insert into route_types values (1100, 'Air Service');
insert into route_types values (1200, 'Ferry Service');
insert into route_types values (1300, 'Aerial Lift Service');
insert into route_types values (1400, 'Funicular Service');
insert into route_types values (1500, 'Taxi Service');
insert into route_types values (1501, 'Communal Taxi Service');
insert into route_types values (1502, 'Water Taxi Service');
insert into route_types values (1503, 'Rail Taxi Service');
insert into route_types values (1504, 'Bike Taxi Service');
insert into route_types values (1505, 'Licensed Taxi Service');
insert into route_types values (1506, 'Private Hire Service Vehicle');
insert into route_types values (1507, 'All Taxi Services');
insert into route_types values (1700, 'Miscellaneous Service');
insert into route_types values (1702, 'Horse-drawn Carriage');

create table payment_methods (
    payment_method  int  primary key
    ,description    text not null
);

insert into payment_methods values (0,'On Board');
insert into payment_methods values (1,'Prepay');

create table transfer_types (
    transfer_type int  primary key
    ,description   text
);

insert into transfer_types values (0,'Preferred transfer point');
insert into transfer_types values (1,'Designated transfer point');
insert into transfer_types values (2,'Transfer possible with min_transfer_time window');
insert into transfer_types values (3,'Transfers forbidden');

--------------------------------------------------------------------------------
-- Tables to populate
--------------------------------------------------------------------------------
create table agency (
    agency_id           text not null--Agency ID er text!
    ,agency_name        text not null
    ,agency_url         text not null -- TODO check if is an URL
    ,agency_timezone    text not null
    ,agency_lang        text
    ,agency_phone       text
    ,agency_fare_url    text          -- TODO check if is an URL
    ,agency_email       text
    ,feed_id            text references feed
    ,primary key (agency_id, feed_id)
);

create table stops (
    stop_id              text not null -- there can be a lot of zero front padded
    ,stop_code           text
    ,stop_name           text  -- TODO conditionally required
    ,tts_stop_name       text
    ,stop_desc           text
    ,stop_lat            double precision -- TODO check if mandatory
    ,stop_lon            double precision -- TODO check if mandatory
    ,zone_id             text
    ,stop_url            text
    ,location_type       int  references location_types
    ,parent_station      text
    ,stop_timezone       text -- TODO
    ,wheelchair_boarding int  references wheelchair_boardings
    ,level_id            text -- TODO make a FK
    ,platform_code       text -- TODO
    ,feed_id             text references feed
    ,primary key (stop_id, feed_id)
    ,foreign key (parent_station, feed_id) references stops
    -- TODO add geom column
);

create table routes (
    route_id              text not null-- TODO check if check an INT data type
    ,agency_id            text -- TODO check if mandatory
    ,route_short_name     text
    ,route_long_name      text
    ,route_desc           text
    ,route_type           int not null references route_types
    ,route_url            text
    ,route_color          text -- TODO check domain
    ,route_text_color     text  -- TODO check domain
    ,route_sort_order     text -- TODO check omain
    ,continuous_pickup    text -- TODO make FK
    ,continuous_drop_off  text -- TODO make FK
    ,feed_id              text references feed
    ,primary key (route_id, feed_id)
    ,foreign key (agency_id, feed_id) references agency
);

create table calendar (
    service_id    text      not null -- note that it can be zero-padded
    ,monday       smallint  not null check(monday in (0, 1))
    ,tuesday      smallint  not null check(tuesday in (0, 1))
    ,wednesday    smallint  not null check(wednesday in (0, 1))
    ,thursday     smallint  not null check(thursday in (0, 1))
    ,friday       smallint  not null check(friday in (0, 1))
    ,saturday     smallint  not null check(saturday in (0, 1))
    ,sunday       smallint  not null check(sunday in (0, 1))
    ,start_date   date      not null
    ,end_date     date      not null  -- check (start_date < end_date) <- We cannot assume this sometimes it is the same day
    ,feed_id      text references feed
    ,primary key (service_id, feed_id)
);

create table calendar_dates (
    service_id     text
   ,date           date  not null
   ,exception_type int   not null    -- TODO should this be a foreign key?
   ,feed_id        text references feed
   ,primary key(service_id, date, exception_type, feed_id) -- TODO make FK to new exception_type table
   ,foreign key (service_id, feed_id) references calendar
);

create table fare_attributes (
     fare_id           text              not null
    ,price             double precision  not null -- check (price > 0) <--- It is sometimes 0!!
    ,currency_type     text              not null -- TODO FK
    ,payment_method    int references payment_methods
    ,transfers         int -- TODO make FK there is a fixed domain
    ,agency_id         text
    ,transfer_duration int check (transfer_duration > 0)
    ,feed_id           text references feed
    ,primary key (fare_id, feed_id)
    ,foreign key (agency_id, feed_id) references agency
);

create table fare_rules (
    fare_id         text
    ,route_id       text
    ,origin_id      text -- TODO make FK
    ,destination_id text -- TODO make FK
    ,contains_id    text -- TODO make FK
    ,feed_id        text references feed
    ,foreign key (route_id, feed_id) references routes
    ,foreign key (fare_id, feed_id) references fare_attributes
);

create table shapes (
     shape_id            text -- Is not a primary key according to danish gtfs
    ,shape_pt_lat        double precision not null
    ,shape_pt_lon        double precision not null
    ,shape_pt_sequence   int              not null
    ,shape_dist_traveled double precision check (shape_dist_traveled > 0)
    ,feed_id             text references feed
);

create table trips (
     route_id              text
    ,service_id            text
    ,trip_id               text not null
    ,trip_headsign         text
    ,trip_short_name       text
    ,direction_id          int  references directions(direction_id)
    ,block_id              text
    ,shape_id              text
    ,wheelchair_accessible text -- TODO check data type and if FK
    ,bikes_allowed         text -- TODO check data type
    ,feed_id               text references feed
    ,primary key (trip_id, feed_id)
    ,foreign key (service_id, feed_id) references calendar
    ,foreign key (route_id, feed_id) references routes

);

create table stop_times (
    trip_id              text
    ,arrival_time        text
    ,departure_time      text
    ,stop_id             text
    ,stop_headsign       text
    ,stop_sequence       int  not null
    ,pickup_type         int references pickup_dropoff_types(type_id)
    ,drop_off_type       int references pickup_dropoff_types(type_id)
    ,continuous_pickup   text -- TODO FK 
    ,continuous_drop_off text -- TODO FK 
    ,shape_dist_traveled text -- TODO data type
    ,timepoint           text -- TODO data type, FK
    ,feed_id             text references feed
    ,foreign key (stop_id, feed_id) references stops
    ,foreign key (trip_id, feed_id) references trips
);

create table frequencies (
    trip_id       text
    ,start_time   text not null
    ,end_time     text not null  -- check (start_time < end_time) <-- Not an assumption we can make!!!!!
    ,headway_secs int not null check (headway_secs > 0)
    ,exact_times  int -- TODO make FK
    ,feed_id      text references feed
    ,foreign key (trip_id, feed_id) references trips
);

create table transfers (
    from_stop_id        text
    ,to_stop_id         text
    ,transfer_type      int  references transfer_types(transfer_type)
    ,min_transfer_time  int  check (min_transfer_time > 0)
    ,feed_id            text references feed
    ,foreign key (from_stop_id, feed_id) references stops
    ,foreign key (to_stop_id, feed_id) references stops
);

create table feed_info (
    feed_publisher_name  text not null
    ,feed_publisher_url  text not null
    ,feed_lang           text not null -- TODO make FK 
    ,default_lang        text          -- TODO make FK
    ,feed_start_date     date
    ,feed_end_date       date
    ,feed_version        text
    ,feed_contact_email  text
    ,feed_contact_url    text
    ,feed_id             text references feed
);

create table attributions (
    attribution_id      text
    ,agency_id          text
    ,route_id           text
    ,trip_id            text
    ,organization_name  text
    ,is_producer        int check(is_producer in (0, 1))
    ,is_operator        int check(is_operator in (0, 1))
    ,is_authority       int check(is_authority in (0, 1))
    ,attribution_url    text
    ,attribution_email  text
    ,attribution_phone  text
    ,feed_id            text references feed
    ,primary key (attribution_id, feed_id)
);

create table levels (
    level_id      text
    ,level_index  float
    ,level_name   text
    ,feed_id      text references feed
    ,primary key (level_id, feed_id)
);


create table pathways (
    pathway_id                  text
    ,from_stop_id               text
    ,to_stop_id                 text
    ,pathway_mode               int
    ,is_bidirectional           int
    ,length                     float
    ,traversal_time             int
    ,stair_count                int
    ,max_slope                  float
    ,min_width                  text
    ,signposted_as              text
    ,reversed_signposted_as     text
    ,feed_id      text references feed
    ,primary key (pathway_id, feed_id)
    ,foreign key (from_stop_id, feed_id) references stops
    ,foreign key (to_stop_id, feed_id) references stops
);


create table translations (
    table_name      text
    ,field_name     text
    ,language       text
    ,translation    text
    ,record_id      text
    ,record_sub_id  text
    ,field_value    text
    ,feed_id        text references feed
);
