import os
import glob
import zipfile
from argparse import ArgumentParser, Namespace

from gtfs_dw.validator.validator import get_feed

temp_dir = "gtfs_dw/temp"


def extract_zip_to_temp(path_to_zip):
    try:
        with zipfile.ZipFile(path_to_zip, 'r') as zip_ref:
            zip_ref.extractall(path=temp_dir)
    except Exception as e:
        print(e)


def clear_temp():
    files = glob.glob(temp_dir + '/*')
    for f in files:
        os.remove(f)


def zipdirec(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file),
                       os.path.relpath(os.path.join(root, file),
                                       os.path.join(path, '../..')))


def zip_files_in_temp():
    zipf = zipfile.ZipFile(temp_dir + '/gtfs.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdirec(temp_dir, zipf)
    zipf.close()


def get_feed_from_temp():
    return get_feed(temp_dir)


def get_str_from_file(path):
    try:
        with open(path, encoding='utf-8') as f:
            return f.read()
    except FileNotFoundError as e:
        print(e)
        return None


def standard_attribute_extract(line, attribute):
    res = ""
    if attribute in line:
        res = line[attribute]
    if " " + attribute in line:
        res = line[" " + attribute]
    if '"' + attribute + '"' in line:
        res = line['"' + attribute + '"']
    if res is None or res == '' or res.replace("\"", "") == '':
        res = 'null'
    if '\ufeff' + attribute in line:
        res = line['\ufeff' + attribute]
    return res.replace("\"", "")


def standard_text_attribute_extract(line, attribute):
    attribute = standard_attribute_extract(line, attribute)
    # Sometimes there are space in front when there shouldn't be!
    if attribute[0] == ' ':
        attribute = attribute[1:]
    if attribute != 'null' and attribute != '' and attribute != ' ':
        attribute = attribute.replace("\'", "")
        return "'" + attribute + "'"
    return 'null'


def standard_number_attribute_extract(line, attribute):
    attribute = standard_attribute_extract(line, attribute)
    if attribute == '' or attribute == ' ':
        attribute = 'null'
    return attribute


def standard_date_attribute_extract(line, attribute):
    attribute = standard_attribute_extract(line, attribute)
    if attribute == 'null':
        return attribute
    # Sometimes there are space in front when there shouldn't be!
    if attribute[0] == ' ':
        attribute = attribute[1:]
    year = attribute[0:4]
    month = attribute[4:6]
    day = attribute[6:8]
    return f"'{year}-{month}-{day}'"


if __name__ == "__main__":
    from arg_parser import make_argparser

    parser: ArgumentParser = make_argparser()
    args: Namespace = parser.parse_args()

    if args.extract:
        extract_zip_to_temp(args.input)

    if args.clear:
        clear_temp()

    if args.zip:
        zip_files_in_temp()
