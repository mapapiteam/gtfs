from argparse import ArgumentParser


def make_argparser() -> ArgumentParser:
    """Return argument parser"""
    arg_parser = ArgumentParser(
        description="extract zip to temp.\n"
    )
    arg_parser.add_argument("input", help="input path GTFS data zip file.")
    arg_parser.add_argument("-e", "--extract", help="extract the files.", action="store_true")
    arg_parser.add_argument("-c", "--clear", help="clear temp dir.", action="store_true")
    arg_parser.add_argument("-z", "--zip", help="zip temp dir.", action="store_true")
    return arg_parser
