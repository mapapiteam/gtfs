from argparse import ArgumentParser


def make_argparser() -> ArgumentParser:
    """Return argument parser"""
    arg_parser = ArgumentParser(
        description="Run main operations on the database.\n"
    )
    arg_parser.add_argument("-c", "--create", help="Create necessary tables for the DB.", action="store_true")
    arg_parser.add_argument("-d", "--drop", help="Drop all tables.", action="store_true")
    arg_parser.add_argument("-r", "--reset", help="First runs --create and then --drop", action="store_true")
    arg_parser.add_argument("-v", "--validate", help="Try to validate and clean a path_or_url before loading", action="store_true")
    arg_parser.add_argument("-u", "--url", help="The input is not a file path but an url", action="store")
    arg_parser.add_argument("-m", "--multi", help="Assumes the input is a text file location. The text file contains atleast one row with path/url to load.", action="store")
    return arg_parser
