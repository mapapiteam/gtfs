import unittest
from gtfs_dw.loader.loaders.fare_attributes import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'agency_id': '539', 'fare_id': '1831', 'price': '1.75', 'currency_type': 'USD', 'payment_method': '0', 'transfers': '1', 'transfer_duration': '120'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into fare_attributes values (\'1831\',1.75,\'USD\',0,1,\'539\',120);')


if __name__ == '__main__':
    unittest.main()
