import unittest

from gtfs_dw.loader.loaders.calendar import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'"service_id"': '1', '"monday"': '0', '"tuesday"': '0', '"wednesday"': '0', '"thursday"': '0', '"friday"': '0', '"saturday"': '0', '"sunday"': '0', '"start_date"': '20210607', '"end_date"': '20210901'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into calendar values (\'1\',0,0,0,0,0,0,0,\'2021-06-07\',\'2021-09-01\');')

    def test_extract_sql_from_line_2(self):
        line = {'"service_id"': '2', '"monday"': '0', '"tuesday"': '0', '"wednesday"': '0', '"thursday"': '0', '"friday"': '0', '"saturday"': '1', '"sunday"': '1', '"start_date"': '20210607', '"end_date"': '20210901'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into calendar values (\'2\',0,0,0,0,0,1,1,\'2021-06-07\',\'2021-09-01\');')


if __name__ == '__main__':
    unittest.main()
