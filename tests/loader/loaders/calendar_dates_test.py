import unittest

from gtfs_dw.loader.loaders.calendar_dates import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'"service_id"': '1', '"date"': '20210607', '"exception_type"': '1'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into calendar_dates values (1,\'2021-06-07\',1);')

    def test_extract_sql_from_line_2(self):
        line = {'"service_id"': '1', '"date"': '20210614', '"exception_type"': '1'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into calendar_dates values (1,\'2021-06-14\',1);')


if __name__ == '__main__':
    unittest.main()
