import unittest

from gtfs_dw.loader.loaders.feed_info import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'feed_publisher_name': 'Alilaguna S.p.A.', 'feed_publisher_url': 'https://www.alilaguna.it/attuale/alilaguna.zip', 'feed_lang': 'it', 'feed_contact_email': 'transit@alilaguna.it'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into feed_info values (\'Alilaguna S.p.A.\',\'https://www.alilaguna.it/attuale/alilaguna.zip\',\'it\',null,null,null,null,\'transit@alilaguna.it\',null);')


if __name__ == '__main__':
    unittest.main()
