import unittest

from gtfs_dw.loader.loaders.fare_rules import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'fare_id': 'F1', 'route_id': 'B', 'origin_id': 'AERO', 'destination_id': 'TRON'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into fare_rules values (\'F1\',\'B\',\'AERO\',\'TRON\',null);')

    def test_extract_sql_from_line_2(self):
        line = {'fare_id': 'F1', 'route_id': 'B', 'origin_id': 'AERO', 'destination_id': 'FERF30'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into fare_rules values (\'F1\',\'B\',\'AERO\',\'FERF30\',null);')


if __name__ == '__main__':
    unittest.main()
