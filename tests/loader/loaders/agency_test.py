import unittest

from gtfs_dw.loader.loaders.agency import extract_sql_from_line


class MyTestCase(unittest.TestCase):

    def test_extract_sql_from_line_1(self):
        line = {'"agency_id"': '202', '"agency_name"': '"Bornholms Trafik"', '"agency_url"': '"https://bat.dk"',
                '"agency_timezone"': '"Europe/Berlin"', '"agency_lang"': '"da"', '"agency_phone"': '""'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into agency values (\'202\',\'Bornholms Trafik\',\'https://bat.dk\',\'Europe/Berlin\',\'da\',null,null,null);')

    def test_extract_sql_from_line_2(self):
        line = {'"agency_id"': '206', '"agency_name"': '"NT"', '"agency_url"': '"https://www'
                                                                               '.nordjyllandstrafikselskab.dk"',
                '"agency_timezone"': '"Europe/Berlin"', '"agency_lang"': '""', '"agency_phone"': '""'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into agency values (\'206\',\'NT\',\'https://www.nordjyllandstrafikselskab.dk\',\'Europe/Berlin\',null,null,null,null);')

if __name__ == '__main__':
    unittest.main()
