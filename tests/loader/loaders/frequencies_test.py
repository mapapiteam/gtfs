import unittest

from gtfs_dw.loader.loaders.frequencies import extract_sql_from_line


class MyTestCase(unittest.TestCase):
    def test_extract_sql_from_line_1(self):
        line = {'trip_id': 'CITY1', 'start_time': '8:00:00', 'end_time': '9:59:59', 'headway_secs': '600'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into frequencies values (\'CITY1\',\'8:00:00\',\'9:59:59\',600,null);')

    def test_extract_sql_from_line_2(self):
        line = {'trip_id': 'CITY2', 'start_time': '6:00:00', 'end_time': '7:59:59', 'headway_secs': '1800'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into frequencies values (\'CITY2\',\'6:00:00\',\'7:59:59\',1800,null);')

    def test_extract_sql_from_line_3(self):
        line = {'trip_id': 'CITY1', 'start_time': '6:00:00', 'end_time': '7:59:59', 'headway_secs': '1800'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into frequencies values (\'CITY1\',\'6:00:00\',\'7:59:59\',1800,null);')

    def test_extract_sql_from_line_4(self):
        line = {'trip_id': 'STBA', 'start_time': '6:00:00', 'end_time': '22:00:00', 'headway_secs': '1800'}
        sql = extract_sql_from_line(line)
        self.assertEqual(sql, 'insert into frequencies values (\'STBA\',\'6:00:00\',\'22:00:00\',1800,null);')


if __name__ == '__main__':
    unittest.main()
